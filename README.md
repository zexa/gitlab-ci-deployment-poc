# gitlab-ci-deployment-poc
This repository is a proof of concept for deploying services with gitlab ci
while maintaining the core functionality which we get from webistrano plus
queues.

The desired functionality:
* Single concurrent deployments
* Locking deployments
* Queues
* Rollbacking

## Process
1. curl is used to get the $DEPLOYMENT variable
2. If the $DEPLOYMENT variable is not set, it is created with value "NONE"
3. If the $DEPLOYNENT variable's value is not "NONE", sleep for
   $DEPLOYMENT_SLEEP seconds (30s by default) and go back to step 1
4. Curl is used to set $DEPLOYMENT to $CI_JOB_URL ("MANUAL" by default)
5. Execute deployment
6. Set $DEPLOYMENT to "None"

## Single concurrent deployments
By using the above process, a deployment will never start if the project's 
$DEPLOYMENT value is not set to "NONE".

The deployment will simply restart over and over again and eventually the job
itself will timeout.

## Locking Deployments
Technically, a lock can be achieved by setting the $DEPLOYMENT variable to
anything but "NONE".

The problem arises when you want to grant the ability to lock deployments for
any developers without write access to a protected branch. You can't set
variables easily.

One solution would be to create a third party service that would use the gitlab
oauth and api for unsetting locks and etc.

One cool feature would be that these locks would be visible in the readme by
help of [embedded status images]
(https://docs.travis-ci.com/user/status-images/)

Another, which I personally think is very hacky, would be to deploy a pipeline
once that would allow you to place/delete locks, then deploy the regular 
pipeline and then use the lock pipeline to lock/unlock jobs by retrying the
lock pipeline.

Or to just include the lock job on every single pipeline. :shrug:

## Queues
Multiple pipelines can be active, but by using the above described process a
fairly clean implementation of FIFO is achieved in this case.

## Rollbacks
As far as UI goes, see (Locking Deployments).

Same functionality as currently exists in webistrano can be achieved by the use
of "Deployment Profiles".

We can predefine a few profiles that will execute specific commands as
currently defined in webistrano.





